/*
*@Author: lyf
*@Date: 2019-07-21 09:56:38
 * @Last Modified by: lyf
 * @Last Modified time: 2019-07-22 21:32:42
*/

/**
 * 处理scss 和 css  HTML
 */
let path = require('path')
let HtmlWebpackPlugin = require('html-webpack-plugin')
let MiniCssExtractPlugins = require('mini-css-extract-plugin')
module.exports = {
  devServer: {
    // 开发服务器配置
    port: 3000,
    // progress: true, // 进度条
    contentBase: './build', 
    compress: true,
  },
  mode: 'development', // development 和 production
  entry: './src/index.js', // 入口
  output: {
    filename: 'bundle.[hash:8].js', // 打包后的文件名
    path: path.resolve(__dirname, 'build'),// 路径必须是绝对路径
  },
  plugins: [
    // 数组放着所有的插件
    new HtmlWebpackPlugin({
      template: './src/index.html',
      filename: 'index.html',
      hash: true
    }),
    // 处理css压缩
    new MiniCssExtractPlugins({
      filename: 'main.css',
    })
  ],
  module: { // 模块
    rules: [ // 规则 
      // css-loader接续@import语法
      // style-loader 把css插入到head的标签中
      // loader的特点 希望单一
      // loader的用法，字符串只用一个loader
      // 多个loader需要[]
      // loader的顺序，默认从右向左执行，
      // loader可以写成对象模式
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugins.loader,
          'css-loader',
          'postcss-loader'
        ]
      },
      // 可以处理less, scss stylus
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugins.loader,
          'css-loader',
          'sass-loader', // 把scss转化为css
          'postcss-loader',
        ]
      }
    ]
  }
}